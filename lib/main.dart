import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Aplikasi Pertama',
        home: Scaffold(
            appBar:AppBar(
              title: Text('aplikasi pertama'),
              backgroundColor: Colors.red,
              leading: Icon(Icons.exit_to_app),
              actions: <Widget>[
                Icon(Icons.thumb_up),
                Icon(Icons.thumb_down),
              ],

            ),
            body: Column(
              children: <Widget>[
                Image.network('https://mahasiswa.undiksha.ac.id/data/foto/2d2143bbfe563de30eb1e875780e94b120180723060736.jpg'),
                Text(
                  'Sukariana Yasa',
                  style: TextStyle(fontSize: 28, fontFamily:"Serif",height: 2.0),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[

                  ],)
              ],
            )
        )
    );
  }
}
